## Front Page Content

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and can be built in under 1 minute.
Literally. It uses the `beautifulhugo` theme which supports content on your front page.
Edit `/content/_index.md` to change what appears here. Delete `/content/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/instruction-citoyenne-strasbourg%2Finstruction-citoyenne-strasbourg.gitlab.io/master?urlpath=lab%2Ftree%2Fnotebooks%2Ftemplate.ipynb)

<iframe width="100%" height="800px" src="https://mybinder.org/v2/gl/instruction-citoyenne-strasbourg%2Finstructions-citoyenne-strasbourg.gitlab.io/master?urlpath=lab%2Ftree%2Fnotebooks%2Ftemplate.ipynb" />